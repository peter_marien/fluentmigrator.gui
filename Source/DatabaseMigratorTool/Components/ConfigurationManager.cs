﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using DatabaseMigratorTool.Configuration;
using DatabaseMigratorTool.Dtos;

namespace DatabaseMigratorTool.Components
{
    public static class ConfigurationManager
    {
        public static List<MigrationNamespace> GetMigrationNamespaces()
        {
            var migratorConfigurationSection = System.Configuration.ConfigurationManager.GetSection(MigratorConfigurationSection.SectionName) as MigratorConfigurationSection;

            var namespaceList = new List<MigrationNamespace>();

            if (migratorConfigurationSection != null)
            {
                foreach (MigrationNamespaceElement element in migratorConfigurationSection.MigrationNamespaces as MigrationNamespacesCollection)
                {
                    var myNamespace = new MigrationNamespace()
                    {
                        Name = element.Name,
                        Path = element.Path
                    };
                    namespaceList.Add(myNamespace);
                }
            }

            return namespaceList;
        }


        public static List<ServerConfiguration> GetServerConfigurations()
        {
            var configurationList = new List<ServerConfiguration>();

            foreach (ConnectionStringSettings setting in System.Configuration.ConfigurationManager.ConnectionStrings)
            {
                var connectionString = new SqlConnectionStringBuilder(setting.ConnectionString);

                var serverConfig = new ServerConfiguration()
                {
                    Name = setting.Name,
                    Server = connectionString.DataSource,
                    Database = connectionString.InitialCatalog,
                    UserId = connectionString.UserID,
                    Password = connectionString.Password,
                    IntegratedSecurity = connectionString.IntegratedSecurity
                };

                configurationList.Add(serverConfig);
            }

            return configurationList;
        }
    }
}
