﻿#region usings

using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DatabaseMigratorTool.Components;
using DatabaseMigratorTool.Dtos;
using DatabaseMigratorTool.Views;
using FastColoredTextBoxNS;
using Migrations.Components;

#endregion

namespace DatabaseMigratorTool.Conrollers
{
    public class MainController
    {
        private readonly MainForm _view;

        private DatabaseManager _databaseManager;

        public MainController()
        {
            _view = new MainForm();
            SubscribeEvents();
            InitDatabaseManager();

            _view.MigrationNamespaces = ConfigurationManager.GetMigrationNamespaces();
            _view.ServerConfigurations = ConfigurationManager.GetServerConfigurations();
        }

        public Form Run()
        {
            _view.StartAsyncJob(false, false);
            _view.Disconnected();
            _view.Show();
            return _view;
        }

        private void InitDatabaseManager()
        {
            _databaseManager = new DatabaseManager();
            _databaseManager.OnMigrationLogging += DatabaseManagerOnMigrationLogging;
            _databaseManager.OnCustomLogging += DatabaseManagerOnCustomLogging;
        }

        private void DatabaseManagerOnMigrationLogging(string message)
        {
            switch (message.Trim())
            {
                case "/* Beginning Transaction */":
                    message = "BEGIN TRANSACTION;";
                    break;
                case "/* Committing Transaction */":
                    message = "COMMIT TRANSACTION;";
                    break;
                case "/* Rolling back transaction */":
                    message = "ROLLBACK TRANSACTION;";
                    break;
            }

            _view.AddToMigratorOutputLog(message);
        }

        private void DatabaseManagerOnCustomLogging(string message)
        {
            _view.AddToLog(message);
        }

        private void SubscribeEvents()
        {
            _view.OnConnectButtonClick += ViewOnConnectButtonClick;
            _view.OnFinishButtonClick += ViewRunMigrationButtonClick;
            _view.OnServerConfigurationChange += ViewOnServerConfigurationChange;
            _view.OnRefreshMigrationTargetListButtonClick += ViewOnRefreshButtonClick;
            _view.OnSaveMigratorOutputClick += ViewOnSaveMigratorOutputClick;
            _view.OnMigratorLogTextChanged += (sender, e) => ViewOnMigratorLogTextChanged(e);

        }

        private void ViewOnMigratorLogTextChanged(TextChangedEventArgs e)
        {
            InitializeHighlightingFoldingMarkers(e);
        }

        private static void InitializeHighlightingFoldingMarkers(TextChangedEventArgs e)
        {
             //clear folding markers
            e.ChangedRange.ClearFoldingMarkers();

            //set folding markers
            e.ChangedRange.SetFoldingMarkers("BEGIN TRANSACTION", "COMMIT TRANSACTION"); //allow to collapse Transactions
            e.ChangedRange.SetFoldingMarkers("BEGIN TRANSACTION", "ROLLBACK TRANSACTION"); //allow to collapse Transactions
            e.ChangedRange.SetFoldingMarkers(@"/\*", @"\*/"); //allow to collapse comment block
        }

        private void ViewOnRefreshButtonClick()
        {
            UpdateExistingMigrationsList();
        }

        private void ViewOnServerConfigurationChange()
        { 
            ServerConfiguration selectedConfiguration = _view.SelectedServerConfiguration;
            
            if (selectedConfiguration!=null)
            {
                _view.Server = selectedConfiguration.Server;
                _view.Database = selectedConfiguration.Database;
                _view.UserId = selectedConfiguration.UserId;
                _view.Password = selectedConfiguration.Password;
                _view.IntegratedSecurity = selectedConfiguration.IntegratedSecurity;

                _view.AddToLog("Server Configuration => " + selectedConfiguration);
            }

            Disconnect();
        }


        private void ViewOnSaveMigratorOutputClick()
        {
            var saveFileDialog = new SaveFileDialog { Filter = @"SQL Files|*.sql|Text Files|*.txt|All Files|*.*" };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string rtf = _view.MigratorOutputLogText;
                File.WriteAllText(saveFileDialog.FileName, rtf);
            }
        }

        private void ViewOnConnectButtonClick()
        {
            if (_databaseManager.IsConnected)
            {
                Disconnect();
            }
            else
            {
                Connect();
            }
        }

        private void Connect()
        {
            if (_view.Server == string.Empty)
            {
                _view.AddToLog(@"Please select a server!");
                MessageBox.Show(@"Please select a server!", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (!_view.IntegratedSecurity && (_view.UserId == string.Empty || _view.Password == string.Empty))
            {
                _view.AddToLog(@"Please enter User Login and User Password!");
                MessageBox.Show(@"Please enter User Login and User Password! ", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            _view.StartAsyncJob(false, true);
            Task.Factory.StartNew(() => _databaseManager.Connect(_view.UserId, _view.Password, _view.Server, _view.IntegratedSecurity)).ContinueWith(result =>
                {
                    _view.StartAsyncJob(false, false);
                    if (result.Exception != null)
                    {
                        _view.AddToLog("Connection failed; " + result.Exception.InnerException.Message);
                        return;
                    }
                    _view.Connected();

                    UpdateExistingMigrationsList();

                }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void Disconnect()
        {
            _databaseManager.Disconnect();
            _view.Disconnected();
        }

        private void ViewRunMigrationButtonClick()
        {
            if (_view.IsMigrateToLatestVersionOperationType)
            {
                MigrateDatabaseToLatestVersion();
                return;
            }
            if (_view.IsRollBackMigrationOperationType)
            {
                RollbackMigrations();
                return;
            }
            if (_view.IsMigrateDownToVersionOperationType)
            {
                MigrateToSpecificVersion();
                return;
            }
            _view.AddToLog(@"Please select operation type!");
            MessageBox.Show(@"Please select operation type!", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

 

        #region DATABASE MIGRATIONS

        private void MigrateDatabaseToLatestVersion()
        {
            var database = _view.Database;
            MigrationNamespace selectedMigrationNamespace = _view.SelectedigrationNamespace;

            // Validate input
            if (!ValidateDatabase(database)) return;
            if (!ValidateNameSpace(selectedMigrationNamespace)) return;

            MigrateToLatestVersionTask(_view.Database, selectedMigrationNamespace.Path);
        }

        private void RollbackMigrations()
        {
            var database = _view.Database;
            MigrationNamespace selectedMigrationNamespace = _view.SelectedigrationNamespace;

            // Validate input
            if (!ValidateDatabase(database)) return;
            if (!ValidateNameSpace(selectedMigrationNamespace)) return;

            MigrateToVersionTask(_view.Database, selectedMigrationNamespace.Path, 0);
        }

        private void UpdateExistingMigrationsList()
        {          
            var database = _view.Database;
            MigrationNamespace selectedMigrationNamespace = _view.SelectedigrationNamespace;
           
            // Validate input
            if (!ValidateDatabase(database)) return;
            if (!ValidateNameSpace(selectedMigrationNamespace)) return;

            SetExistingMigrationList(database, selectedMigrationNamespace.Path);
        }

        private void MigrateToSpecificVersion()
        {
            string database = _view.Database;
            MigrationNamespace selectedMigrationNamespace = _view.SelectedigrationNamespace;
            MigrationInformation migrationTargetInformation = _view.SelectedMigrationTargetVersion;
            bool isTargetSelected = _view.IsMigrationTargetSelected;

            // Validate input
            if (!ValidateDatabase(database)) return;
            if (!ValidateNameSpace(selectedMigrationNamespace)) return;
            if (!ValidateMigrationTargetInformation( isTargetSelected, migrationTargetInformation)) return;

            MigrateToVersionTask(database, selectedMigrationNamespace.Path, migrationTargetInformation.Version);
        }

        private void MigrateToLatestVersionTask(string dbname, string migrationNamespace)
        {
            _view.StartAsyncJob(false, true);
            Task.Factory.StartNew(() => _databaseManager.MigrateToLatestVersion(dbname, migrationNamespace))
                .ContinueWith(result =>
                {
                    _view.StartAsyncJob(false, false);
                    if (result.Exception != null)
                    {
                        string exceptionMessage = "Migration database" + dbname + " to latest version failed; ";
                        _view.AddToLog(exceptionMessage + result.Exception.InnerException.Message);
                        return;
                    }
                    
                    UpdateExistingMigrationsList();

                }, CancellationToken.None, 
                   TaskContinuationOptions.None,
                   TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void SetExistingMigrationList(string dbname, string migrationNamespace)
        {
            _view.StartAsyncJob(false, true);
            Task.Factory.StartNew(() => _databaseManager.GetMigrationVersions(dbname, migrationNamespace))
                .ContinueWith(result =>
                {
                    _view.StartAsyncJob(false, false);
                    if (result.Exception != null)
                    {
                        string exceptionMessage = string.Format("Getting existing migrations on database [{0}] failed; ", dbname);
                        _view.AddToLog(exceptionMessage + result.Exception.InnerException.Message);
                        return;
                    }
                    
                    var migrationInfoList = result.Result;
                    _view.ExistingMigrations = migrationInfoList.Select(x => new MigrationInformation { Version = x.Key, Description = x.Value}).ToList();

                    _view.ResetSelectedMigrationTarget();

                }, TaskScheduler.FromCurrentSynchronizationContext());

        }

        private void MigrateToVersionTask(string dbname, string migrationNamespace, long version)
        {
            _view.StartAsyncJob(false, true);

            Task.Factory.StartNew(() => _databaseManager.MigrateToSpecificVersion(dbname, migrationNamespace, version))
                .ContinueWith(result =>
                {
                    _view.StartAsyncJob(false, false);
                    if (result.Exception != null)
                    {
                        string exceptionMessage = string.Format("Migration on database [{0}] to specific version [{1}] failed; ", dbname, version);
                        _view.AddToLog(exceptionMessage + result.Exception.InnerException.Message);
                        return;
                    }
                   
                    UpdateExistingMigrationsList();

                }, CancellationToken.None, 
                   TaskContinuationOptions.None,
                   TaskScheduler.FromCurrentSynchronizationContext());
        }

        #endregion


        #region VALIDATION OF INPUT

        private bool ValidateDatabase(string database)
        {
            if (database == string.Empty)
            {
                _view.AddToLog(@"Please select database!");
                MessageBox.Show(@"Please select database!", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private bool ValidateMigrationTargetInformation(bool isTargetSelected, MigrationInformation migrationTargetInformation)
        {
            if (!isTargetSelected || migrationTargetInformation == null)
            {
                const string targetVersionErrorMsg = @"Please select a targetversion for the migration!";
                _view.AddToLog(targetVersionErrorMsg);
                MessageBox.Show(targetVersionErrorMsg, @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        private bool ValidateNameSpace(MigrationNamespace selectedMigrationNamespace)
        {
            if (selectedMigrationNamespace == null)
            {
                _view.AddToLog(@"Please select migration namespace!");
                MessageBox.Show(@"Please select migration namespace!", @"Warning", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return false;
            }
            return true;
        }

        #endregion

    }
}
