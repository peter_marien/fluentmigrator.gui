using System.Configuration;

namespace DatabaseMigratorTool.Configuration
{
    public class MigrationNamespacesCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new MigrationNamespaceElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MigrationNamespaceElement)element).Name;
        }
    }
}