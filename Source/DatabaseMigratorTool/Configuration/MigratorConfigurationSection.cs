﻿using System.Configuration;

namespace DatabaseMigratorTool.Configuration
{
    public class MigratorConfigurationSection : ConfigurationSection
    {
        /// <summary>
        /// The name of this section in the app.config.
        /// </summary>
        public const string SectionName = "MigratorConfigurationSection";

        private const string MigrationNamespaceCollectionName = "MigrationNamespaces";

        [ConfigurationProperty(MigrationNamespaceCollectionName)]
        [ConfigurationCollection(typeof(MigrationNamespacesCollection), AddItemName = "add")]
        public MigrationNamespacesCollection MigrationNamespaces { get { return (MigrationNamespacesCollection)base[MigrationNamespaceCollectionName]; } }
    }
}