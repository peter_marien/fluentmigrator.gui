﻿namespace DatabaseMigratorTool.Dtos
{
    public class MigrationInformation
    {
        public long Version { get; set; }
        public string Description { get; set; }
        
        public override string ToString()
        {
            return string.Format("{0} - {1}", Version, Description==string.Empty ? "[N/A]" : Description);
        }
    }
}