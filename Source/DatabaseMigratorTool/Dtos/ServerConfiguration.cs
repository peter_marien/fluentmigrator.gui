﻿namespace DatabaseMigratorTool.Dtos
{
    public class ServerConfiguration
    {
        public string Name { get; set; }
        public string Server { get; set; }
        public string Database { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public bool IntegratedSecurity { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - [{1} - {2}]", Name, Server, Database);
        }
    }
}