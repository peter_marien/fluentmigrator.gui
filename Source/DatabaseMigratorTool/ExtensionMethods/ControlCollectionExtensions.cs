﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DatabaseMigratorTool.ExtensionMethods
{
    public static class ControlCollectionExtensions
    {
        private static Dictionary<Type, Action<Control>> controldefaults =
            new Dictionary<Type, Action<Control>>
            { 
               {typeof(TextBox), c => ((TextBox)c).Clear()},
               {typeof(CheckBox), c => ((CheckBox)c).Checked = false},
               {typeof(ListBox), c => ((ListBox)c).Items.Clear()},
               {typeof(RadioButton), c => ((RadioButton)c).Checked = false},
               {typeof(ComboBox), c =>((ComboBox) c).SelectedIndex=-1},
               {typeof(GroupBox), c => c.Controls.ResetControls()},
               {typeof(Panel), c => c.Controls.ResetControls()}
            };

        private static void FindAndInvoke(Type type, Control control)
        {
            if (controldefaults.ContainsKey(type))
            {
                controldefaults[type].Invoke(control);
            }
        }

        /// <summary>
        /// Method that will recursively clear all controls in the ControlCollection
        /// </summary>
        /// <param name="controllCollection">Collection of Controls to clear</param>
        /// <param name="recursively">optional parameter recursively clear child controls</param>
        public static void ResetControls(this Control.ControlCollection controllCollection, bool recursively=true)
        {
            foreach (Control control in controllCollection)
            {
                FindAndInvoke(control.GetType(), control);

                if (recursively && control.HasChildren)
                {
                    control.Controls.ResetControls();
                }
            }
        }

        /// <summary>
        /// Generic method that will recursively clear all the controls of type T
        /// </summary>
        /// <typeparam name="T">Type of Control</typeparam>
        /// <param name="controllCollection">Collection of Controls to clear</param>
        /// <param name="recursively">optional parameter recursively clear child controls</param>
        public static void ResetControls<T>(this Control.ControlCollection controllCollection, bool recursively=true) where T : Control
        {
            if (!controldefaults.ContainsKey(typeof(T))) return;

            foreach (Control control in controllCollection)
            {
                if (control.GetType() == typeof(T))
                {
                    FindAndInvoke(typeof(T), control);
                }

                if (recursively && control.HasChildren)
                {
                    control.Controls.ResetControls<T>();
                }
            }

        }

    }
}
