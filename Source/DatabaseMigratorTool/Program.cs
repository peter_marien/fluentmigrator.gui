﻿#region usings

using System;
using System.Diagnostics;
using System.Windows.Forms;
using DatabaseMigratorTool.Conrollers;

#endregion

namespace DatabaseMigratorTool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            AppDomain.CurrentDomain.UnhandledException += (sender, e) => HandleError((Exception)e.ExceptionObject);
            Application.ThreadException += (sender, e) => HandleError(e.Exception);

            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var mainController = new MainController();
            Application.Run(mainController.Run());
        }


        private static void HandleError(Exception exception)
        {
            try
            {
                if (exception.Message.ToLower().Contains("timeout"))
                {
                    MessageBox.Show(@"There's a problem with the network connection to the database server! \n (Connection Timeout)", @"Bug...",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                new ErrorHandlerController(exception).Run();
            }
            catch (Exception e)
            {

                MessageBox.Show(@"Error processing errors: \n \n" + e);
                if (MessageBox.Show(@"Attach a debugger?? \n Developer!!!", @"Debugging...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Debugger.Launch();
                    throw;
                }
            }

        }
    }
}
