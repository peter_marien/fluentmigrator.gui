﻿using DatabaseMigratorTool.Dtos;
using FastColoredTextBoxNS;

namespace DatabaseMigratorTool.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.connectionGroupBox = new System.Windows.Forms.GroupBox();
            this.lblDataSource = new System.Windows.Forms.Label();
            this.txtDatabase = new System.Windows.Forms.TextBox();
            this.lblServerConfiguration = new System.Windows.Forms.Label();
            this.cmbServerConfigurations = new System.Windows.Forms.ComboBox();
            this.serverConfigurationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnConnect = new System.Windows.Forms.Button();
            this.chkIntegratedSecurityLogin = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.migrationNamespaceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.operationGroupBox = new System.Windows.Forms.GroupBox();
            this.btnRunMigration = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbtRollbackMigrations = new System.Windows.Forms.RadioButton();
            this.cmbMigrationTargetVersion = new System.Windows.Forms.ComboBox();
            this.existingMigrationInformationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnRefreshMigrationTargetList = new System.Windows.Forms.Button();
            this.rbtMigrateToSpecificVersion = new System.Windows.Forms.RadioButton();
            this.rbtMigrateToLatestVersion = new System.Windows.Forms.RadioButton();
            this.txtApplicationLog = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnClearApplicationLog = new System.Windows.Forms.Button();
            this.tabControlLogging = new System.Windows.Forms.TabControl();
            this.tabPageApplicationLogging = new System.Windows.Forms.TabPage();
            this.tabPageMigratorLogging = new System.Windows.Forms.TabPage();
            this.btnSaveMigratorOutput = new System.Windows.Forms.Button();
            this.txtMigratorLog = new FastColoredTextBoxNS.FastColoredTextBox();
            this.btnClearMigratorLog = new System.Windows.Forms.Button();
            this.connectionGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serverConfigurationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.migrationNamespaceBindingSource)).BeginInit();
            this.operationGroupBox.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.existingMigrationInformationBindingSource)).BeginInit();
            this.tabControlLogging.SuspendLayout();
            this.tabPageApplicationLogging.SuspendLayout();
            this.tabPageMigratorLogging.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMigratorLog)).BeginInit();
            this.SuspendLayout();
            // 
            // connectionGroupBox
            // 
            this.connectionGroupBox.Controls.Add(this.lblDataSource);
            this.connectionGroupBox.Controls.Add(this.txtDatabase);
            this.connectionGroupBox.Controls.Add(this.lblServerConfiguration);
            this.connectionGroupBox.Controls.Add(this.cmbServerConfigurations);
            this.connectionGroupBox.Controls.Add(this.btnConnect);
            this.connectionGroupBox.Controls.Add(this.chkIntegratedSecurityLogin);
            this.connectionGroupBox.Controls.Add(this.label3);
            this.connectionGroupBox.Controls.Add(this.txtPassword);
            this.connectionGroupBox.Controls.Add(this.label2);
            this.connectionGroupBox.Controls.Add(this.txtUserId);
            this.connectionGroupBox.Controls.Add(this.lblServer);
            this.connectionGroupBox.Controls.Add(this.txtServer);
            this.connectionGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectionGroupBox.Location = new System.Drawing.Point(9, 13);
            this.connectionGroupBox.Name = "connectionGroupBox";
            this.connectionGroupBox.Size = new System.Drawing.Size(475, 235);
            this.connectionGroupBox.TabIndex = 0;
            this.connectionGroupBox.TabStop = false;
            this.connectionGroupBox.Text = "Connection to MSSQL Server";
            // 
            // lblDataSource
            // 
            this.lblDataSource.AutoSize = true;
            this.lblDataSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataSource.Location = new System.Drawing.Point(39, 84);
            this.lblDataSource.Name = "lblDataSource";
            this.lblDataSource.Size = new System.Drawing.Size(56, 13);
            this.lblDataSource.TabIndex = 13;
            this.lblDataSource.Text = "Database:";
            // 
            // txtDatabase
            // 
            this.txtDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDatabase.Location = new System.Drawing.Point(117, 81);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(223, 20);
            this.txtDatabase.TabIndex = 12;
            // 
            // lblServerConfiguration
            // 
            this.lblServerConfiguration.AutoSize = true;
            this.lblServerConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServerConfiguration.Location = new System.Drawing.Point(6, 22);
            this.lblServerConfiguration.Name = "lblServerConfiguration";
            this.lblServerConfiguration.Size = new System.Drawing.Size(72, 13);
            this.lblServerConfiguration.TabIndex = 11;
            this.lblServerConfiguration.Text = "Configuration:";
            // 
            // cmbServerConfigurations
            // 
            this.cmbServerConfigurations.DataSource = this.serverConfigurationBindingSource;
            this.cmbServerConfigurations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbServerConfigurations.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbServerConfigurations.FormattingEnabled = true;
            this.cmbServerConfigurations.Location = new System.Drawing.Point(84, 19);
            this.cmbServerConfigurations.Name = "cmbServerConfigurations";
            this.cmbServerConfigurations.Size = new System.Drawing.Size(380, 21);
            this.cmbServerConfigurations.TabIndex = 10;
            // 
            // serverConfigurationBindingSource
            // 
            this.serverConfigurationBindingSource.DataSource = typeof(DatabaseMigratorTool.Dtos.ServerConfiguration);
            // 
            // btnConnect
            // 
            this.btnConnect.Image = global::DatabaseMigratorTool.Properties.Resources.Connect;
            this.btnConnect.Location = new System.Drawing.Point(9, 194);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(455, 32);
            this.btnConnect.TabIndex = 4;
            this.btnConnect.Text = "Connect";
            this.btnConnect.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConnect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // chkIntegratedSecurityLogin
            // 
            this.chkIntegratedSecurityLogin.AutoSize = true;
            this.chkIntegratedSecurityLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIntegratedSecurityLogin.Location = new System.Drawing.Point(58, 107);
            this.chkIntegratedSecurityLogin.Name = "chkIntegratedSecurityLogin";
            this.chkIntegratedSecurityLogin.Size = new System.Drawing.Size(121, 17);
            this.chkIntegratedSecurityLogin.TabIndex = 1;
            this.chkIntegratedSecurityLogin.Text = "Use Integrated login";
            this.chkIntegratedSecurityLogin.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(39, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Password:";
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(117, 156);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(223, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(39, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "User Login:";
            // 
            // txtUserId
            // 
            this.txtUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserId.Location = new System.Drawing.Point(117, 130);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(223, 20);
            this.txtUserId.TabIndex = 2;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServer.Location = new System.Drawing.Point(39, 58);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(41, 13);
            this.lblServer.TabIndex = 1;
            this.lblServer.Text = "Server:";
            // 
            // txtServer
            // 
            this.txtServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServer.Location = new System.Drawing.Point(117, 55);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(223, 20);
            this.txtServer.TabIndex = 0;
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.migrationNamespaceBindingSource;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(9, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(444, 21);
            this.comboBox1.TabIndex = 0;
            // 
            // migrationNamespaceBindingSource
            // 
            this.migrationNamespaceBindingSource.DataSource = typeof(DatabaseMigratorTool.Dtos.MigrationNamespace);
            // 
            // operationGroupBox
            // 
            this.operationGroupBox.Controls.Add(this.btnRunMigration);
            this.operationGroupBox.Controls.Add(this.groupBox4);
            this.operationGroupBox.Controls.Add(this.groupBox3);
            this.operationGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.operationGroupBox.Location = new System.Drawing.Point(9, 316);
            this.operationGroupBox.Name = "operationGroupBox";
            this.operationGroupBox.Size = new System.Drawing.Size(475, 295);
            this.operationGroupBox.TabIndex = 1;
            this.operationGroupBox.TabStop = false;
            this.operationGroupBox.Text = "Migrator Settings";
            // 
            // btnRunMigration
            // 
            this.btnRunMigration.Image = global::DatabaseMigratorTool.Properties.Resources.Finish;
            this.btnRunMigration.Location = new System.Drawing.Point(6, 248);
            this.btnRunMigration.Name = "btnRunMigration";
            this.btnRunMigration.Size = new System.Drawing.Size(459, 32);
            this.btnRunMigration.TabIndex = 12;
            this.btnRunMigration.Text = "Run Migration";
            this.btnRunMigration.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRunMigration.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRunMigration.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(6, 25);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(459, 49);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Migration Namespace";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbtRollbackMigrations);
            this.groupBox3.Controls.Add(this.cmbMigrationTargetVersion);
            this.groupBox3.Controls.Add(this.btnRefreshMigrationTargetList);
            this.groupBox3.Controls.Add(this.rbtMigrateToSpecificVersion);
            this.groupBox3.Controls.Add(this.rbtMigrateToLatestVersion);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(6, 92);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(459, 119);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Operation Type";
            // 
            // rbtRollbackMigrations
            // 
            this.rbtRollbackMigrations.AutoSize = true;
            this.rbtRollbackMigrations.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtRollbackMigrations.Location = new System.Drawing.Point(6, 42);
            this.rbtRollbackMigrations.Name = "rbtRollbackMigrations";
            this.rbtRollbackMigrations.Size = new System.Drawing.Size(118, 17);
            this.rbtRollbackMigrations.TabIndex = 12;
            this.rbtRollbackMigrations.TabStop = true;
            this.rbtRollbackMigrations.Text = "Rollback Migrations";
            this.rbtRollbackMigrations.UseVisualStyleBackColor = true;
            // 
            // cmbMigrationTargetVersion
            // 
            this.cmbMigrationTargetVersion.DataSource = this.existingMigrationInformationBindingSource;
            this.cmbMigrationTargetVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMigrationTargetVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMigrationTargetVersion.FormattingEnabled = true;
            this.cmbMigrationTargetVersion.Location = new System.Drawing.Point(25, 88);
            this.cmbMigrationTargetVersion.Name = "cmbMigrationTargetVersion";
            this.cmbMigrationTargetVersion.Size = new System.Drawing.Size(389, 21);
            this.cmbMigrationTargetVersion.TabIndex = 11;
            // 
            // existingMigrationInformationBindingSource
            // 
            this.existingMigrationInformationBindingSource.DataSource = typeof(DatabaseMigratorTool.Dtos.MigrationInformation);
            // 
            // btnRefreshMigrationTargetList
            // 
            this.btnRefreshMigrationTargetList.Image = global::DatabaseMigratorTool.Properties.Resources.Refresh;
            this.btnRefreshMigrationTargetList.Location = new System.Drawing.Point(420, 81);
            this.btnRefreshMigrationTargetList.Name = "btnRefreshMigrationTargetList";
            this.btnRefreshMigrationTargetList.Size = new System.Drawing.Size(32, 32);
            this.btnRefreshMigrationTargetList.TabIndex = 10;
            this.btnRefreshMigrationTargetList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRefreshMigrationTargetList.UseVisualStyleBackColor = true;
            // 
            // rbtMigrateToSpecificVersion
            // 
            this.rbtMigrateToSpecificVersion.AutoSize = true;
            this.rbtMigrateToSpecificVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtMigrateToSpecificVersion.Location = new System.Drawing.Point(6, 65);
            this.rbtMigrateToSpecificVersion.Name = "rbtMigrateToSpecificVersion";
            this.rbtMigrateToSpecificVersion.Size = new System.Drawing.Size(155, 17);
            this.rbtMigrateToSpecificVersion.TabIndex = 10;
            this.rbtMigrateToSpecificVersion.TabStop = true;
            this.rbtMigrateToSpecificVersion.Text = "Migrate Specific To Version";
            this.rbtMigrateToSpecificVersion.UseVisualStyleBackColor = true;
            // 
            // rbtMigrateToLatestVersion
            // 
            this.rbtMigrateToLatestVersion.AutoSize = true;
            this.rbtMigrateToLatestVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtMigrateToLatestVersion.Location = new System.Drawing.Point(6, 19);
            this.rbtMigrateToLatestVersion.Name = "rbtMigrateToLatestVersion";
            this.rbtMigrateToLatestVersion.Size = new System.Drawing.Size(146, 17);
            this.rbtMigrateToLatestVersion.TabIndex = 6;
            this.rbtMigrateToLatestVersion.TabStop = true;
            this.rbtMigrateToLatestVersion.Text = "Migrate To Latest Version";
            this.rbtMigrateToLatestVersion.UseVisualStyleBackColor = true;
            // 
            // txtApplicationLog
            // 
            this.txtApplicationLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtApplicationLog.BackColor = System.Drawing.Color.White;
            this.txtApplicationLog.Enabled = false;
            this.txtApplicationLog.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApplicationLog.Location = new System.Drawing.Point(3, 3);
            this.txtApplicationLog.Multiline = true;
            this.txtApplicationLog.Name = "txtApplicationLog";
            this.txtApplicationLog.ReadOnly = true;
            this.txtApplicationLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtApplicationLog.Size = new System.Drawing.Size(668, 520);
            this.txtApplicationLog.TabIndex = 13;
            this.txtApplicationLog.TabStop = false;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(9, 254);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(475, 25);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar.TabIndex = 9;
            // 
            // btnClearApplicationLog
            // 
            this.btnClearApplicationLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearApplicationLog.Image = global::DatabaseMigratorTool.Properties.Resources.ClearLog;
            this.btnClearApplicationLog.Location = new System.Drawing.Point(518, 529);
            this.btnClearApplicationLog.Name = "btnClearApplicationLog";
            this.btnClearApplicationLog.Size = new System.Drawing.Size(150, 32);
            this.btnClearApplicationLog.TabIndex = 13;
            this.btnClearApplicationLog.Text = "Clear Application Log";
            this.btnClearApplicationLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClearApplicationLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClearApplicationLog.UseVisualStyleBackColor = true;
            // 
            // tabControlLogging
            // 
            this.tabControlLogging.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlLogging.Controls.Add(this.tabPageApplicationLogging);
            this.tabControlLogging.Controls.Add(this.tabPageMigratorLogging);
            this.tabControlLogging.Location = new System.Drawing.Point(490, 13);
            this.tabControlLogging.Name = "tabControlLogging";
            this.tabControlLogging.SelectedIndex = 0;
            this.tabControlLogging.Size = new System.Drawing.Size(682, 598);
            this.tabControlLogging.TabIndex = 15;
            // 
            // tabPageApplicationLogging
            // 
            this.tabPageApplicationLogging.Controls.Add(this.btnClearApplicationLog);
            this.tabPageApplicationLogging.Controls.Add(this.txtApplicationLog);
            this.tabPageApplicationLogging.Location = new System.Drawing.Point(4, 22);
            this.tabPageApplicationLogging.Name = "tabPageApplicationLogging";
            this.tabPageApplicationLogging.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageApplicationLogging.Size = new System.Drawing.Size(674, 572);
            this.tabPageApplicationLogging.TabIndex = 0;
            this.tabPageApplicationLogging.Text = "Application Logging";
            this.tabPageApplicationLogging.UseVisualStyleBackColor = true;
            // 
            // tabPageMigratorLogging
            // 
            this.tabPageMigratorLogging.Controls.Add(this.btnSaveMigratorOutput);
            this.tabPageMigratorLogging.Controls.Add(this.txtMigratorLog);
            this.tabPageMigratorLogging.Controls.Add(this.btnClearMigratorLog);
            this.tabPageMigratorLogging.Location = new System.Drawing.Point(4, 22);
            this.tabPageMigratorLogging.Name = "tabPageMigratorLogging";
            this.tabPageMigratorLogging.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMigratorLogging.Size = new System.Drawing.Size(674, 572);
            this.tabPageMigratorLogging.TabIndex = 1;
            this.tabPageMigratorLogging.Text = "MigratorLogging";
            this.tabPageMigratorLogging.UseVisualStyleBackColor = true;
            // 
            // btnSaveMigratorOutput
            // 
            this.btnSaveMigratorOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveMigratorOutput.Image = global::DatabaseMigratorTool.Properties.Resources.Save;
            this.btnSaveMigratorOutput.Location = new System.Drawing.Point(6, 531);
            this.btnSaveMigratorOutput.Name = "btnSaveMigratorOutput";
            this.btnSaveMigratorOutput.Size = new System.Drawing.Size(155, 32);
            this.btnSaveMigratorOutput.TabIndex = 16;
            this.btnSaveMigratorOutput.Text = "Export Output";
            this.btnSaveMigratorOutput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveMigratorOutput.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSaveMigratorOutput.UseVisualStyleBackColor = true;
            // 
            // txtMigratorLog
            // 
            this.txtMigratorLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMigratorLog.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.txtMigratorLog.AutoScrollMinSize = new System.Drawing.Size(25, 14);
            this.txtMigratorLog.BackBrush = null;
            this.txtMigratorLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMigratorLog.CharHeight = 14;
            this.txtMigratorLog.CharWidth = 7;
            this.txtMigratorLog.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMigratorLog.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtMigratorLog.Font = new System.Drawing.Font("Consolas", 9F);
            this.txtMigratorLog.IsReplaceMode = false;
            this.txtMigratorLog.Location = new System.Drawing.Point(3, 3);
            this.txtMigratorLog.Name = "txtMigratorLog";
            this.txtMigratorLog.Paddings = new System.Windows.Forms.Padding(0);
            this.txtMigratorLog.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtMigratorLog.Size = new System.Drawing.Size(668, 520);
            this.txtMigratorLog.TabIndex = 0;
            this.txtMigratorLog.Zoom = 100;
            // 
            // btnClearMigratorLog
            // 
            this.btnClearMigratorLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearMigratorLog.Image = global::DatabaseMigratorTool.Properties.Resources.ClearLog;
            this.btnClearMigratorLog.Location = new System.Drawing.Point(518, 529);
            this.btnClearMigratorLog.Name = "btnClearMigratorLog";
            this.btnClearMigratorLog.Size = new System.Drawing.Size(150, 32);
            this.btnClearMigratorLog.TabIndex = 15;
            this.btnClearMigratorLog.Text = "Clear Migrator Log";
            this.btnClearMigratorLog.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClearMigratorLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClearMigratorLog.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 662);
            this.Controls.Add(this.tabControlLogging);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.operationGroupBox);
            this.Controls.Add(this.connectionGroupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 575);
            this.Name = "MainForm";
            this.Text = "FluentMigrator Tool";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.connectionGroupBox.ResumeLayout(false);
            this.connectionGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serverConfigurationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.migrationNamespaceBindingSource)).EndInit();
            this.operationGroupBox.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.existingMigrationInformationBindingSource)).EndInit();
            this.tabControlLogging.ResumeLayout(false);
            this.tabPageApplicationLogging.ResumeLayout(false);
            this.tabPageApplicationLogging.PerformLayout();
            this.tabPageMigratorLogging.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMigratorLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox connectionGroupBox;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.CheckBox chkIntegratedSecurityLogin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.GroupBox operationGroupBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbtMigrateToLatestVersion;
        private System.Windows.Forms.TextBox txtApplicationLog;
        private System.Windows.Forms.Button btnRunMigration;
        private System.Windows.Forms.Button btnRefreshMigrationTargetList;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource migrationNamespaceBindingSource;
        private System.Windows.Forms.Label lblServerConfiguration;
        private System.Windows.Forms.ComboBox cmbServerConfigurations;
        private System.Windows.Forms.BindingSource serverConfigurationBindingSource;
        private System.Windows.Forms.Label lblDataSource;
        private System.Windows.Forms.TextBox txtDatabase;
        private System.Windows.Forms.ComboBox cmbMigrationTargetVersion;
        private System.Windows.Forms.RadioButton rbtMigrateToSpecificVersion;
        private System.Windows.Forms.BindingSource existingMigrationInformationBindingSource;
        private System.Windows.Forms.RadioButton rbtRollbackMigrations;
        private System.Windows.Forms.Button btnClearApplicationLog;
        private System.Windows.Forms.TabControl tabControlLogging;
        private System.Windows.Forms.TabPage tabPageApplicationLogging;
        private System.Windows.Forms.TabPage tabPageMigratorLogging;
        private System.Windows.Forms.Button btnClearMigratorLog;

        private FastColoredTextBox txtMigratorLog;
        private System.Windows.Forms.Button btnSaveMigratorOutput;
    }
}

