﻿#region usings

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DatabaseMigratorTool.Dtos;
using DatabaseMigratorTool.ExtensionMethods;
using FastColoredTextBoxNS;

#endregion

namespace DatabaseMigratorTool.Views
{
    public partial class MainForm : Form
    {
        public Action OnConnectButtonClick;
        public Action OnFinishButtonClick;
        public Action OnRefreshMigrationTargetListButtonClick;
        public Action OnServerConfigurationChange;
        public Action OnSaveMigratorOutputClick;
        public Action <object, TextChangedEventArgs> OnMigratorLogTextChanged;


        public MainForm()
        {
            InitializeComponent();
            SubscribeEvents();
            InitializeSyntaxHighlighting();
        }

        private void InitializeSyntaxHighlighting()
        {
            txtMigratorLog.Language=Language.SQL;
            txtMigratorLog.TextChanged+= (sender, e) => OnMigratorLogTextChanged(sender, e);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ResetServerConfiguration();
            txtApplicationLog.Clear();
        }

        private void ResetServerConfiguration()
        {
            cmbServerConfigurations.SelectedIndex = -1;
            connectionGroupBox.Controls.ResetControls();
        }

        private void SubscribeEvents()
        {
            btnConnect.Click += (sender, e) => OnConnectButtonClick();
            btnRunMigration.Click += (sender, e) => OnFinishButtonClick();
            btnRefreshMigrationTargetList.Click += (sender, e) => OnRefreshMigrationTargetListButtonClick();
            btnSaveMigratorOutput.Click += (sender, e) => OnSaveMigratorOutputClick();
            btnClearApplicationLog.Click += (sender, e) => OnClearApplicationLogClick();
            btnClearMigratorLog.Click += (sender, e) => OnClearMigratorLogClick();
            chkIntegratedSecurityLogin.CheckedChanged += (sender, e) => useIntegratedLoginCheckBox_Click();
            cmbServerConfigurations.SelectedIndexChanged += (sender, e) => OnServerConfigurationChange();
        }

        private void OnClearApplicationLogClick()
        {
            txtApplicationLog.Clear();
        } 
        
        private void OnClearMigratorLogClick()
        {
            txtMigratorLog.Clear();
        }


        private void useIntegratedLoginCheckBox_Click()
        {
            txtPassword.Enabled = txtUserId.Enabled = !chkIntegratedSecurityLogin.Checked;
        }

        #region Server Configuration

        public ServerConfiguration SelectedServerConfiguration
        {
            get
            {
                // For some reason the bindingSource.Current isn't always updated ??? => update manually
                serverConfigurationBindingSource.Position = cmbServerConfigurations.SelectedIndex;
                return serverConfigurationBindingSource.Current as ServerConfiguration; 
            }
        }

        public string Server
        {
            get { return txtServer.Text; }
            set { txtServer.Text = value; }
        }

        public string Database
        {
            get { return txtDatabase.Text; }
            set { txtDatabase.Text = value; }
        }

        public string UserId
        {
            get { return txtUserId.Text; }
            set { txtUserId.Text = value; }
        }

        public string Password
        {
            get { return txtPassword.Text; }
            set { txtPassword.Text = value; }
        }

        public bool IntegratedSecurity
        {
            get { return chkIntegratedSecurityLogin.Checked; }
            set { chkIntegratedSecurityLogin.Checked = value; }
        }

        #endregion


        public bool IsMigrateDownToVersionOperationType
        {
            get { return rbtMigrateToSpecificVersion.Checked; }
        }      

        public bool IsMigrateToLatestVersionOperationType
        {
            get { return rbtMigrateToLatestVersion.Checked; }
        }

        public bool IsRollBackMigrationOperationType
        {
            get { return rbtRollbackMigrations.Checked; }
        }

        public bool IsMigrationTargetSelected
        {
            get { return cmbMigrationTargetVersion.SelectedIndex > -1; }
        }

        public List<MigrationNamespace> MigrationNamespaces
        {
            set { migrationNamespaceBindingSource.DataSource = value; }
            get { return migrationNamespaceBindingSource.DataSource as List<MigrationNamespace>; }
        }

        public List<MigrationInformation> ExistingMigrations
        {
            set { existingMigrationInformationBindingSource.DataSource = value; }
            get { return existingMigrationInformationBindingSource.DataSource as List<MigrationInformation>; }
        }
        
        public List<ServerConfiguration> ServerConfigurations
        {
            set { serverConfigurationBindingSource.DataSource = value; }
            get { return serverConfigurationBindingSource.DataSource as List<ServerConfiguration>; }
        }

        public MigrationNamespace SelectedigrationNamespace
        {
            get { return migrationNamespaceBindingSource.Current as MigrationNamespace; }
        }
        
        public MigrationInformation SelectedMigrationTargetVersion
        {
            get { return existingMigrationInformationBindingSource.Current as MigrationInformation; }
        }

        public string MigratorOutputLogText
        {
            get { return txtMigratorLog.Text; }
        }

        public void Connected()
        {
            btnConnect.Text = "Disconnect";
            operationGroupBox.Enabled = true;
        }

        public void Disconnected()
        {
            btnConnect.Text = "Connect";
            operationGroupBox.Enabled = false;
        }

        public void AddToLog(string message)
        {
            if(!string.IsNullOrEmpty(message))
            {
                if (message == "\r\n")
                {
                    txtApplicationLog.Invoke((MethodInvoker)(() => txtApplicationLog.AppendText(message)));
                }
                else
                {
                    txtApplicationLog.Invoke((MethodInvoker)(() => txtApplicationLog.AppendText(DateTime.Now + @": " + message + "\r\n")));
                }
            }
        }

        public void AddToMigratorOutputLog(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                if (message == "\r\n")
                {
                    txtMigratorLog.Invoke((MethodInvoker)(() => txtMigratorLog.AppendText(message)));
                }
                else
                {
                    txtMigratorLog.Invoke((MethodInvoker)(() => txtMigratorLog.AppendText(message + Environment.NewLine)));
                }
            }
        }

        public void StartAsyncJob(bool isUsePercent, bool isVisible)
        {
            progressBar.Style = isUsePercent ? ProgressBarStyle.Blocks : ProgressBarStyle.Marquee;
            progressBar.Visible = isVisible;
            connectionGroupBox.Enabled = operationGroupBox.Enabled = !isVisible;
        }

        public void ResetSelectedMigrationTarget()
        {
            cmbMigrationTargetVersion.SelectedIndex = -1;
        }

     
    }
}
