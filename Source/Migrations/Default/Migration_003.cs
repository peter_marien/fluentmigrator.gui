﻿#region usings




#endregion


using FluentMigrator;
using Migrations.Components;
using Migrations.Configuration;
using Migrations.MigrationExtensions;

namespace Migrations.Default
{
    [Migration(3, "Author: Peter Marien; Create user table")]
    public class Migration_003: Migration
    {

        public override void Up()
        {
            if(MigrationHelper.GetDataSource(this)==DataSources.LocalHost)
            {
                Create.Table(Tables.Users3)
                      .WithPrimaryKeyColumn()
                      .WithColumn("Name").AsString().NotNullable()
                      .WithColumn("FirstName").AsString().NotNullable()
                      .WithColumn("Email").AsString().NotNullable()
                      .WithColumn("Address").AsString().NotNullable()
                      .WithColumn("City").AsString().NotNullable()
                      .WithColumn("PostalCode").AsString().NotNullable()
                      ;
            }
        }

        public override void Down()
        {
            if (MigrationHelper.GetDataSource(this) == DataSources.LocalHost)
            {
                Delete.Table(Tables.Users3);
            }
        }
    }
}
