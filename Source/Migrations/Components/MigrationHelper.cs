﻿using System.Data.SqlClient;
using FluentMigrator;

namespace Migrations.Components
{
    public static class MigrationHelper
    {
        public static string GetDatabaseName(Migration migration)
        {
            var builder = new SqlConnectionStringBuilder(migration.ConnectionString);
            return builder.InitialCatalog;
        }

        public static string GetDataSource(Migration migration)
        {
            var builder = new SqlConnectionStringBuilder(migration.ConnectionString);
            return builder.DataSource;
        }
    }
}