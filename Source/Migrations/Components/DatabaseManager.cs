﻿#region usings

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors.SqlServer;
using Microsoft.SqlServer.Management.Common;

#endregion

namespace Migrations.Components
{
    public class MigrationOptions : IMigrationProcessorOptions
    {
        public bool PreviewOnly { get; set; }
        public int Timeout { get; set; }
        public string ProviderSwitches { get; private set; }
    }

    public class DatabaseManager
    {
        public Action<string> OnMigrationLogging;
        public Action<string> OnCustomLogging;
        public bool IsConnected { get; private set; }
        private ServerConnection _connection;
        private string _userName;
        private string _password;
        private string _serverName;
        private bool _useInteratedLogin;

        public void Connect(string userName, string password, string serverName, bool useInteratedLogin)
        {
            _userName = userName;
            _password = password;
            _serverName = serverName;
            _useInteratedLogin = useInteratedLogin;

            if (useInteratedLogin)
            {
                var sqlCon = new SqlConnection(string.Format("Data Source={0}; Integrated Security=True; Connection Timeout=5", serverName));
                _connection = new ServerConnection(sqlCon);
                _connection.Connect();
                IsConnected = true;
            }
            else
            {
                _connection = new ServerConnection(serverName, userName, password);
                _connection.ConnectTimeout = 5000;
                _connection.Connect();
                IsConnected = true;
            }

            OnCustomLogging("Connected");
        }

        public void Disconnect()
        {
            if (IsConnected)
            {
                _connection.Disconnect();
                OnCustomLogging("Disconnected");
            }

            IsConnected = false;
        }

        private string GetConnectionString(string databaseName)
        {
            if (!IsConnected)
                throw new Exception("Connection does not exist, connect first");

            return _useInteratedLogin
                ? string.Format("data source={0};initial catalog={1};integrated security=True;MultipleActiveResultSets=True", _serverName, databaseName)
                : string.Format("data source={0};initial catalog={1};integrated security=False;User ID={2};Password={3} ;MultipleActiveResultSets=True", _serverName, databaseName, _userName, _password);
        }


        #region DATABASE MIGRATIONS

        private MigrationRunner GetMigrationRunner(string databaseName, string migrationNamespace)
        {
            var announcer = new TextWriterAnnouncer(OnMigrationLogging);
            announcer.ShowSql = true;
            
            var assembly = Assembly.GetExecutingAssembly();

            var migrationContext = new RunnerContext(announcer)
            {
                Namespace = migrationNamespace
            };

            var options = new MigrationOptions {PreviewOnly = false, Timeout = 60};
            var factory = new SqlServer2008ProcessorFactory();
            var processor = factory.Create(GetConnectionString(databaseName), announcer, options);
            var runner = new MigrationRunner(assembly, migrationContext, processor);
            return runner;
        }

        public void MigrateToLatestVersion(string databaseName, string migrationNamespace)
        {
            var runner = GetMigrationRunner(databaseName, migrationNamespace);

            //runner.SilentlyFail = true;
            //runner.ListMigrations();

            runner.MigrateUp(true);
            OnCustomLogging("Migrate To Latest version Succeeds");
        }

        public void MigrateToSpecificVersion(string databaseName, string migrationNamespace, long targetVersion)
        {
            var runner = GetMigrationRunner(databaseName, migrationNamespace);

            //runner.SilentlyFail = true;
            //runner.ListMigrations();

            if (runner.VersionLoader.VersionInfo.Latest() > targetVersion)
            {
                runner.MigrateDown(targetVersion);
            }
            else
            {
                runner.MigrateUp(targetVersion);
            }

            OnCustomLogging(string.Format("Migrate To Specific Version with number [{0}] succeeds", targetVersion));
        }


        public List<KeyValuePair<long,string>> GetMigrationVersions(string databaseName, string migrationNamespace)
        {
            var runner = GetMigrationRunner(databaseName, migrationNamespace);

            var migrationList= runner.MigrationLoader.LoadMigrations();

            var returnList = new List<KeyValuePair<long, string>>();

            foreach (var migrationInfo in migrationList)
            {
                bool isNew = !runner.VersionLoader.VersionInfo.HasAppliedMigration(migrationInfo.Key);
                long key = migrationInfo.Key;
                string description = isNew
                    ? "[New] - " + migrationInfo.Value.Description
                    : migrationInfo.Value.Description;

                returnList.Add(new KeyValuePair<long, string>(key, description));
            }

//            OnCustomLogging("Done");

            return returnList;
        }

        #endregion

    }
}
