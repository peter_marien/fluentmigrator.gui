﻿using FluentMigrator;
using FluentMigrator.Builders.Alter.Table;
using FluentMigrator.Builders.Create.Table;

namespace Migrations.MigrationExtensions
{
    public static class CreateTableExtensions
    {
        #region As type extensions

        public static ICreateTableColumnOptionOrWithColumnSyntax AsMaxString(this ICreateTableColumnAsTypeSyntax createTableColumnAsTypeSyntax)
        {
            return createTableColumnAsTypeSyntax.AsString(int.MaxValue);
        }

        #endregion



        #region Create table extensions

        public static ICreateTableWithColumnSyntax WithCompositePrimaryKeyColumn(this ICreateTableWithColumnSyntax rootsySyntax, string[] columnNames)
        {
            foreach (var column in columnNames)
            {
                rootsySyntax.WithColumn(column)
                    .AsInt64()
                    .PrimaryKey();
            }

            return rootsySyntax;
        }

        public static ICreateTableWithColumnSyntax WithForeignKeyColumnNotNullable(this ICreateTableWithColumnSyntax rootsSyntax, string foreignKeyTableName)
        {
            return rootsSyntax.WithColumn(foreignKeyTableName + "Id")
                .AsInt64().NotNullable()
                .ForeignKey(foreignKeyTableName, "Id");
        }

        public static ICreateTableWithColumnSyntax WithForeignKeyColumnNullable(this ICreateTableWithColumnSyntax rootSyntax, string foreignKeyTableName)
        {
            return rootSyntax.WithColumn(foreignKeyTableName + "Id")
                .AsInt64()
                .Nullable()
                .ForeignKey(foreignKeyTableName, "Id");
        }

        public static ICreateTableWithColumnSyntax WithPrimaryKeyColumn(this ICreateTableWithColumnSyntax rootSyntax)
        {
            return rootSyntax.WithColumn("Id")
                .AsInt64()
                .NotNullable()
                .PrimaryKey();
        }

        public static ICreateTableWithColumnSyntax WithPrimaryKeyIdentityColumn(this ICreateTableWithColumnSyntax rootSyntax)
        {
            return rootSyntax.WithColumn("Id")
                .AsInt64()
                .NotNullable()
                .PrimaryKey()
                .Identity();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithTimeStamps(this ICreateTableWithColumnSyntax rootSyntax)
        {
            return rootSyntax.WithColumn("CreatedAt").AsDateTime().NotNullable()
                .WithColumn("ModifiedAt").AsDateTime().NotNullable();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithIdAsInt32(this ICreateTableWithColumnSyntax rootSyntax)
        {
            return rootSyntax
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithIdAsInt64(this ICreateTableWithColumnSyntax rootsSyntax)
        {
            return rootsSyntax
                .WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity();
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithIsDeletedColumn(this ICreateTableWithColumnSyntax rootsSyntax)
        {
            return rootsSyntax
                .WithColumn("IsDeleted").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithCreatedAtColumn(this ICreateTableWithColumnSyntax rootsSyntax)
        {
            return rootsSyntax
                .WithColumn("CreatedAt").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);
        }

        public static ICreateTableColumnOptionOrWithColumnSyntax WithIsActiveColumn(this ICreateTableWithColumnSyntax rootsSyntax, bool defaultValue = true)
        {
            return rootsSyntax
                .WithColumn("IsActive").AsBoolean().NotNullable().WithDefaultValue(defaultValue);
        }

        #endregion



        #region Alter table extensions

        public static IAlterTableColumnOptionOrAddColumnOrAlterColumnSyntax AddIsDeletedColumn(this IAlterTableAddColumnOrAlterColumnSyntax rootsSyntax)
        {
            return rootsSyntax
                .AddColumn("IsDeleted").AsBoolean().NotNullable().WithDefaultValue(false);
        }

        public static IAlterTableColumnOptionOrAddColumnOrAlterColumnSyntax AddCreatedAtColumn(this IAlterTableAddColumnOrAlterColumnSyntax rootsSyntax)
        {
            return rootsSyntax
                .AddColumn("CreatedAt").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentDateTime);
        }

        public static IAlterTableColumnOptionOrAddColumnOrAlterColumnSyntax AddIsActiveColumn(this IAlterTableAddColumnOrAlterColumnSyntax rootsSyntax, bool defaultValue = true)
        {
            return rootsSyntax
                .AddColumn("IsActive").AsBoolean().NotNullable().WithDefaultValue(defaultValue);
        }

        #endregion
    }
}
