# README #

## What is this repository for? ##

This tool is a GUI for the [Fluent Migrations Framework](http://www.github.com/schambers/fluentmigrator)



## How do I get set up? ##

### Setup Visual Studio Solution  ##
- Add the two projects to your solution  
	1. DatabaseMigratorTool  
		This project is used for running the migrations defined in the 'Migrations' project  

	2. Migrations  
		Define your fluentmigration scripts 
	
### Configuration ###

The location of the migrations can be configured in the config-file

      <MigratorConfigurationSection>
    	<MigrationNamespaces>
      	<!-- Multiple Namespaces could be define (e.g. multiple databases) -->
      	<add name="Default" path="Migrations.Default" />
		<add name="My Second Database" path="Migrations.MySecondDatabase"/>
    	</MigrationNamespaces>
  	  </MigratorConfigurationSection>

## Additional Features ##

###  1.	Check datasource  
 
    if (MigrationHelper.GetDataSource(this) == DataSources.LocalHost) 
    {
        Create.Table(Tables.Users)
              .WithPrimaryKeyColumn()
              .WithColumn("Name").AsString().NotNullable()
              .WithColumn("FirstName").AsString().NotNullable()
              ;
    }

###  2.	Check database name 
	
	if (MigrationHelper.GetDatabaseName(this) == Databases.Test_FluentMigrator_GUI)
    {
        Create.Table(Tables.Users)
              .WithPrimaryKeyColumn()
              .WithColumn("Name").AsString().NotNullable()
              .WithColumn("FirstName").AsString().NotNullable()
              ;
    }

###  3.	Bulk Script Execution
	
	ScriptExecutor.ExecuteScriptsFromFolder("FOLDER_NAME" ,Execute);
    
### 4. Several migration types for different databases

You can use this tool for migrate databases with different schema.  

1. Go to **Migrations** project  
2. Create new folder like "**MySecondDatabase**"  
3. Create file with migration class based from **Migration** in namespace **Migrations.MySecondDatabase**
4. Add new migration namespace in config file *(see configuration)*
	